const csrf = require('csurf');
let exceptionHandler = (req, res, next) => {
    const csrfProtection = csrf({ cookie: true, ignoreMethods: ['POST', 'GET'] });
    csrfProtection(req, res, next);
}

module.exports = { exceptionHandler }